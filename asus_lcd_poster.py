#!/usr/bin/env python3
import smbus    # System Management Bus / i2c
import time     # Used to delay when sending signals
import random   # For fun

# Configure to your device, find your device address by using (i2cdetect -y 1)
bus = smbus.SMBus(1)    # 0 = /dev/i2c-0 (port I2C0), 1 = /dev/i2c-1 (port I2C1)
DEVICE_ADDRESS = 0x2F   #7 bit address (will be left shifted to add the read write bit)

# Enable or Disable Backlight
def setLight(state):
    if state == True:
        bus.write_byte_data(DEVICE_ADDRESS, 0x0D, 0xFF)
    else:
        bus.write_byte_data(DEVICE_ADDRESS, 0x0D, 0x00)
    time.sleep(0.02)

# Blink Light - for dramatic effect
def blinkLight(n, delay):
    for count in range(n):
        setLight(False)
        time.sleep(delay)
        setLight(True)
        time.sleep(delay)

# Switch display mode
#       msg = built-in POST messages (boring)
#       timer = uptime timer display mode (needs work, find ways to reset, etc)
#       text = displays registers 0x05 - 0x0C
def setMode(mode):
    if mode == 'msg':
        bus.write_byte_data(DEVICE_ADDRESS, 0x00, 0x00)
    elif mode == 'timer':
        bus.write_byte_data(DEVICE_ADDRESS, 0x00, 0x01)
    elif mode == 'text':
        bus.write_byte_data(DEVICE_ADDRESS, 0x00, 0x02)
    time.sleep(0.02)

# Display 8 ASCII coded characters
#       Currently setting the registers individually, there may be a way to
#       send all 8 at once but I haven't found it
def writeText(data):
    data = data.upper()         # lowercase ASCII characters won't show up
    for msg in range(5, 13):    # iterate the registers 0x05 to 0x0C
        setMode('text')         # update the display mode
        b = msg - 5

        if (b+1) > len(data):
            out = 0
        else:
            out = ord(data[b])

        try:
            bus.write_i2c_block_data(DEVICE_ADDRESS, msg, [out])
            time.sleep(0.01)
        except KeyboardInterrupt:
            break
        except IOError:
            time.sleep(0.01)
            continue
    setMode('text')

# Remove all text from the display
def clearScreen():
    writeText('')

# Scroll text right to left, just for fun and makes it possible to display
# messages longer than 8 characters
def scrollText(data):
    clearScreen()
    data = '       ' + data
    s = 0
    while s < len(data):
        writeText(data[s:])
        s = s + 1
    clearScreen()

## =============================================================== Tests

if __name__=="__main__":
    setLight(False)
    clearScreen()
    time.sleep(1)
    setLight(True)
    scrollText("Imgur...")
    blinkLight(1, 0.5)
    writeText("Sendnudz")
    time.sleep(0.5)
    blinkLight(4, 0.15)
    time.sleep(2)
    clearScreen()
