# ASUS LCD poster display

Drive an ASUS LCD poster from a Raspberry Pi (and maybe an ESP32) using SMBUS.

# License

Uncertain. Original author hasn't given a license. Code itself is short and
simple. What's of real interest are mostly the addresses and learning from the
code should be OK since it was posted in the open.

# Credits

feydkin on https://imgur.com/gallery/UlQWV and https://pastebin.com/j6pi7VZL

## Original post by feydkin

Hacked an LCD POSTER using the Pi. Sharing the info I have and hoping others can contribute (for science). This is my first time doing anything with the GPIO interface - it's fun!

YOU TRY - You'll need the LCD POSTER, Raspberry Pi with raspbian, i2c enabled, and Python with smbus package. There's a ton of info on these things so I won't get into that.

WIRING - LCD POSTER to Pi; red to 5v, black to ground, white to SCL1, green to SDA1

Sending Data to the LCD POSTER - By writing data bytes to registers on the LCD POSTER you can change it's display state. I've figured out the following pieces of the puzzle...

0x00 - Display Mode (0x00 = POST Message, 0x01 = Timer, 0x02 = Text)
0x05 to 0x0C - Text Mode buffer, each register holds an ASCII code (eg. 0x50 = P)
0x0D - Blue backlight (0x00 = OFF, 0xFF = ON)

Looking forward to any information anyone else has! Thanks.
